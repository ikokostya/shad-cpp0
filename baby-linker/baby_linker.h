#pragma once

#include <optional>

#include <object_file.h>

class ErrorReporter {
public:
    virtual void ReportUndefinedSymbol(const std::string& symbol) = 0;
    virtual void ReportMultipleSymbolDefinitions(const std::string& symbol) = 0;
};

std::optional<Executable> Link(const std::vector<LinkerArgument*>& arguments, ErrorReporter* reporter);